﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

using Communicator = Trivia_gui.Classes.Communicator;

namespace Trivia_gui
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private Communicator _communicator;

        [STAThread]
        void App_Startup(object sender, StartupEventArgs e)
        {
            _communicator = new Communicator();
            if (_communicator.IsConnected)
            {
                // Application is running
                // Process command line args
                bool startMinimized = false;
                for (int i = 0; i < e.Args.Length; ++i)
                {
                    if (e.Args[i] == "/StartMinimized")
                    {
                        startMinimized = true;
                    }
                }

                // Create main application window, starting minimized if specified
                MainWindow mainWindow = new MainWindow(_communicator);
                if (startMinimized)
                {
                    mainWindow.WindowState = WindowState.Minimized;
                }
                mainWindow.Show();
            }
            else
            {
                MessageBox.Show("The Connection has failed!\nPlease check your internet connection!");
                Application.Current.Shutdown();
            }
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            _communicator.Exit();
        }
    }
}
