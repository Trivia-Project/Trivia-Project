﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;

using Newtonsoft.Json;

using ICommunicator = Trivia_gui.Interfaces.ICommunicator;

namespace Trivia_gui.Classes
{
    public class Communicator : ICommunicator
    {
        public Config_Json Config { get; protected set; }
        public KeyValuePair<string, string> UserCredentials { get; protected set; }
        public bool IsLoggedIn { get; protected set; } = false;

        private readonly TcpClient _tcpClient;

        public bool IsConnected
        {
            get
            {
                if (_tcpClient != null)
                    return _tcpClient.Connected;
                else
                    return false;
            }
        }

        public Communicator()
        {
            if (File.Exists("config.json"))
            {
                string config = string.Empty;
                using (StreamReader sr = new StreamReader("config.json"))
                {
                    config = sr.ReadToEnd();
                }

                Config = JsonConvert.DeserializeObject<Config_Json>(config);
            }
            else
            {
                Config = new Config_Json();
                using (StreamWriter sw = new StreamWriter("config.json"))
                {
                    sw.Write(JsonConvert.SerializeObject(Config));
                }
            }
            try
            {
                _tcpClient = new TcpClient(Config.host, Config.port);
            }
            catch (Exception)
            {
            }
        }

        public Tout Send<Tin, Tout>(Tin req) where Tin : Request where Tout : Response
        {
            byte[] sendBuffer = req.Serialize();
            byte[] recvBuffer = new byte[1024];
            if (_tcpClient.Connected)
            {
                int sendSize = _tcpClient.Client.Send(sendBuffer);
                if (_tcpClient.Connected)
                {
                    int recvSize = _tcpClient.Client.Receive(recvBuffer);
                    Array.Resize(ref recvBuffer, recvSize);

                    string resRaw = Encoding.ASCII.GetString(recvBuffer);
                    return JsonConvert.DeserializeObject<Tout>(resRaw);
                }
            }
            throw new Exception();
        }

        public void Exit()
        {
            try
            {
                if(IsLoggedIn)
                {
                    Request_Logout req = new Request_Logout { username = UserCredentials.Key };
                    Response_Logout res = TryLogout(req);
                    IsLoggedIn = false;
                    UserCredentials = default(KeyValuePair<string, string>);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                if (_tcpClient != null)
                {
                    if (_tcpClient.Connected)
                        _tcpClient.GetStream().Close();
                    _tcpClient.Close();
                    _tcpClient.Dispose();
                }
            }
        }

        public Response_Login TryLogin(Request_Login req)
            => Send<Request_Login, Response_Login>(req);

        public Response_Logout TryLogout(Request_Logout req)
            => Send<Request_Logout, Response_Logout>(req);

        public Response_Signup TrySignup(Request_Signup req)
            => Send<Request_Signup, Response_Signup>(req);

        public void SetUserCredentials(KeyValuePair<string, string> cred)
        {
            UserCredentials = cred;
            if(cred.Equals(default(KeyValuePair<string, string>)))
            {
                IsLoggedIn = false;
            }
            else
                IsLoggedIn = true;
        }
    }
}
