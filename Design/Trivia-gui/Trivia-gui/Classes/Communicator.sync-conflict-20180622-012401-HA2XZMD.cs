﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;

using Newtonsoft.Json;

using ICommunicator = Trivia_gui.Interfaces.ICommunicator;

namespace Trivia_gui.Classes
{
    public class Communicator : ICommunicator
    {
        public Config_Json Config { get; protected set; }
        public KeyValuePair<string, string> UserCredentials { get; protected set; }
        public bool IsLoggedIn { get; protected set; } = false;

        public Communicator()
        {
            if (File.Exists("config.json"))
            {
                string config = string.Empty;
                using (StreamReader sr = new StreamReader("config.json"))
                {
                    config = sr.ReadToEnd();
                }

                Config = JsonConvert.DeserializeObject<Config_Json>(config);
            }
            else
            {
                Config = new Config_Json();
                using (StreamWriter sw = new StreamWriter("config.json"))
                {
                    sw.Write(JsonConvert.SerializeObject(Config));
                }
            }
        }

        public Tout Send<Tin, Tout>(Tin req) where Tin : Request where Tout : Response
        {

        }

        public Response_Login TryLogin(Request_Login req)
            => Send<Request_Login, Response_Login>(req);

        public Response_Logout TryLogout(Request_Logout req)
            => Send<Request_Logout, Response_Logout>(req);

        public Response_Signup TrySignup(Request_Signup req)
            => Send<Request_Signup, Response_Signup>(req);

        public void SetUserCredentials(KeyValuePair<string, string> cred)
        {
            UserCredentials = cred;
            if(cred.Equals(default(KeyValuePair<string, string>)))
            {
                IsLoggedIn = false;
            }
            else
                IsLoggedIn = true;
        }
    }
}
