﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia_gui.Classes
{
    public class Config_Json
    {
        public string host { get; set; } = "127.0.0.1";
        public int port { get; set; } = 8826;
        public bool encrypt { get; set; } = false;
    }
}
