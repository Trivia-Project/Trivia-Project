﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Trivia_gui.Classes.Controls
{
    public class RoomListViewItem : Control
    {
        public uint Id { get; protected set; }
        public string RoomName { get; protected set; }
        public int PlayersCount
        {
            get { return Players.Count; }
        }
        public List<TextBlock> Players { get; protected set; }

        public RoomListViewItem(uint id, string name, string[] players)
        {
            Id = id;
            RoomName = name;
            Players = new List<TextBlock>();
            foreach (var pl in players)
            {
                Players.Add(new TextBlock
                {
                    Text = pl,
                    FontSize = 12
                });
            }
        }
    }
}
