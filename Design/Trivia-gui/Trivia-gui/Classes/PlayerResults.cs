﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia_gui.Classes
{
    public class PlayerResults
    {
        public string username { get; set; }
        public uint correctAnswerCount { get; set; }
        public uint wrongAnswerCount { get; set; }
        public uint averageAnswerTime { get; set; }
    }
}
