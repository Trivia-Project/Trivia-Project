﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Trivia_gui.Classes
{
    public abstract class Request
    {
        protected abstract Helper.RequestTypes _type { get; }
        public byte[] Serialize()
        {
            string content = JsonConvert.SerializeObject(this);
            int length = content.Length;

            string formattedMessage = $"{(int)(_type)}{length}{content}";
            byte[] buffer = Encoding.ASCII.GetBytes(formattedMessage);
            return buffer;
        }
    }

    public class Request_CreateRoom : Request
    {
        public string roomName { get; set; }
        public uint maxUsers { get; set; }
        public uint questionCount { get; set; }
        public uint answerTimeOut { get; set; }

        protected override Helper.RequestTypes _type => Helper.RequestTypes.CreateRoom;
    }

    public class Request_JoinRoom : Request
    {
        public uint roomId { get; set; }
        public string player { get; set; }

        protected override Helper.RequestTypes _type => Helper.RequestTypes.JoinRoom;
    }

    public class Request_Login : Request
    {
        public string username { get; set; }
        public string password { get; set; }

        protected override Helper.RequestTypes _type => Helper.RequestTypes.Login;
    }

    public class Request_Logout : Request
    {
        public string username { get; set; }

        protected override Helper.RequestTypes _type => Helper.RequestTypes.Logout;
    }

    public class Request_Signup : Request
    {
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }

        protected override Helper.RequestTypes _type => Helper.RequestTypes.Signup;
    }

    public class Request_SubmitAnswer : Request
    {
        public uint answerId { get; set; }

        protected override Helper.RequestTypes _type => Helper.RequestTypes.SubmitAnswer;
    }

    public class Request_GetRooms : Request
    {
        protected override Helper.RequestTypes _type => Helper.RequestTypes.GetRooms;
    }
    public class Request_GetRoomState : Request
    {
        public uint roomId { get; set; }

        protected override Helper.RequestTypes _type => Helper.RequestTypes.GetRoomState;
    }

    public class Request_StartGame : Request
    {
        public uint roomId { get; set; }

        protected override Helper.RequestTypes _type => Helper.RequestTypes.StartGame;
    }

    public class Request_CloseRoom : Request
    {
        public uint roomId { get; set; }

        protected override Helper.RequestTypes _type => Helper.RequestTypes.CloseRoom;
    }

    public class Request_LeaveRoom : Request
    {
        public string username { get; set; }
        public uint roomId { get; set; }

        protected override Helper.RequestTypes _type => Helper.RequestTypes.LeaveRoom;
    }

    public class Request_GetPlayerStats : Request
    {
        public string username { get; set; }

        protected override Helper.RequestTypes _type => Helper.RequestTypes.GetPlayerStats;
    }

    public class Request_GetQuestion : Request
    {
        protected override Helper.RequestTypes _type => Helper.RequestTypes.GetQuestion;
    }

    public class Request_GetHighscores : Request
    {
        protected override Helper.RequestTypes _type => Helper.RequestTypes.GetHighscores;
    }
}
