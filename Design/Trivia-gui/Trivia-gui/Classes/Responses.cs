﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia_gui.Classes
{
    public abstract class Response
    {
        public uint status { get; set; }

        public bool IsSuccess()
            => status == Helper.SUCCESS;
    }

    public class Response_Error : Response
    {
        public string message { get; set; }
    }

    public class Response_CloseRoom : Response
    {
    }

    public class Response_CreateRoom : Response
    {
    }

    public class Response_GetGameResult : Response
    {
        public PlayerResults[] results { get; set; }
    }

    public class Response_GetQuestion : Response
    {
        public string question { get; set; }
        public Dictionary<uint, string> answers { get; set; }
    }

    public class Response_GetRooms : Response
    {
        public RoomData[] rooms { get; set; }
    }

    public class Response_GetRoomState : Response
    {
        public uint questionCount { get; set; }
        public uint answerTimeout { get; set; }
        public bool hasGameBegun { get; set; }
        public string[] players { get; set; }
        public uint maxPlayers { get; set; }
        public string adminName { get; set; }
    }

    public class Response_GetHighscores : Response
    {
        public Dictionary<string, int> highscores { get; set; }
    }

    public class Response_Login : Response
    {
    }

    public class Response_Logout : Response
    {
    }

    public class Response_Signup : Response
    {
    }

    public class Response_StartGame : Response
    {
    }

    public class Response_SubmitAnswer : Response
    {
        public uint correctAnswerId { get; set; }
    }

    public class Response_JoinRoom : Response
    {
    }

    public class Response_LeaveRoom : Response
    {
    }

    public class Response_GetPlayerStats : Response
    {
        public uint gamesNum { get; set; }
        public uint correctAnswersCount { get; set; }
        public uint mistakesCount { get; set; }
        public uint averageAnswerTime { get; set; }
    }
}
