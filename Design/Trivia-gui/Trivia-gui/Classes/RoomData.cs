﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia_gui.Classes
{
    public class RoomData
    {
        public uint id { get; set; }
        public string name { get; set; }
        public uint timePerQuestion { get; set; }
        public uint isActive { get; set; }
    }
}
