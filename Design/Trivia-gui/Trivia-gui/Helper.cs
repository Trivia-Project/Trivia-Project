﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia_gui
{
    public static class Helper
    {
        public const int SUCCESS = 1;
        public const int REFRESH_COOLDOWN = 1000; //1sec

        public static bool IsEmpty(this string str)
            => str == string.Empty;

        public enum RequestTypes
        {
            Login = 200,
            Signup,
            CreateRoom,
            GetRoomState,
            LeaveRoom,
            CloseRoom,
            Logout,
            JoinRoom,
            GetPlayerStats,
            StartGame,
            GetRooms,
            SubmitAnswer,
            GetQuestion,
            GetHighscores
        }
    }
}
