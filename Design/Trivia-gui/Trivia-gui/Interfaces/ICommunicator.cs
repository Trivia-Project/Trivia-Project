﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Trivia_gui.Classes;

using Request = Trivia_gui.Classes.Request;

namespace Trivia_gui.Interfaces
{
    public interface ICommunicator
    {
        Config_Json Config { get; }
        KeyValuePair<string, string> UserCredentials { get; }

        Response_Login TryLogin(Request_Login req);
        Response_Logout TryLogout(Request_Logout req);
        Response_Signup TrySignup(Request_Signup req);

        Tout Send<Tin, Tout>(Tin req) where Tin : Request where Tout : Response;
    }
}
