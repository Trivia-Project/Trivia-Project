﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.IO;

using Trivia_gui.Windows;
using Trivia_gui.Classes;

namespace Trivia_gui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private Visibility _loggedInVisib = Visibility.Hidden;
        public Visibility LoggedInVisibility
        {
            get { return _loggedInVisib; }
            set { _loggedInVisib = value; OnPropertyChanged(); }
        }
        private Communicator _communicator;

        public MainWindow()
        { }

        public MainWindow(Communicator comm)
        {
            InitializeComponent();
            _communicator = comm;

            DataContext = this;
            Login_UpdateGUI();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if(handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        private void Login_UpdateGUI()
        {
            if(_communicator.IsLoggedIn)
            {
                LoggedInVisibility = Visibility.Visible;
                loginFormStk.Visibility = Visibility.Hidden;
                passwordBox.Text = string.Empty;
                usernameBox.Text = string.Empty;
                currUserTxt.Text = $"Welcome {_communicator.UserCredentials.Key}!";
            }
            else
            {
                loginFormStk.Visibility = Visibility.Visible;
                LoggedInVisibility = Visibility.Hidden;
            }
        }

        private void loginBtn_Click(object sender, RoutedEventArgs e)
        {
            string errorMsg = "Login request has failed!";
            try
            {
                if (!_communicator.IsLoggedIn)
                {
                    if (!passwordBox.Text.IsEmpty() && !usernameBox.Text.IsEmpty())
                    {
                        Request_Login req = new Request_Login { username = usernameBox.Text, password = passwordBox.Text };
                        Response_Login res = _communicator.TryLogin(req);
                        if (res != null && res.IsSuccess())
                        {
                            _communicator.SetUserCredentials(new KeyValuePair<string, string>(usernameBox.Text, passwordBox.Text));
                            Login_UpdateGUI();
                        }
                        else
                            MessageBox.Show(errorMsg);
                    }
                    else
                    {
                        MessageBox.Show("Username and Password must not be Empty!");
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show(errorMsg);
            }
        }

        private void joinRoomBtn_Click(object sender, RoutedEventArgs e)
        {
            JoinRoomWindow win = new JoinRoomWindow(_communicator);
            win.Show();
            Close();
        }

        private void createRoomBtn_Click(object sender, RoutedEventArgs e)
        {
            CreateRoomWindow win = new CreateRoomWindow(_communicator);
            win.Show();
            Close();
        }

        private void userStatsBtn_Click(object sender, RoutedEventArgs e)
        {
            StatsWindow win = new StatsWindow(_communicator);
            win.Show();
            Close();
        }

        private void leaderboardBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void signOutBtn_Click(object sender, RoutedEventArgs e)
        {
            string errorMsg = "Logout request has failed!";
            try
            {
                if (_communicator.IsLoggedIn)
                {
                    Request_Logout req = new Request_Logout { username = _communicator.UserCredentials.Key };
                    Response_Logout res = _communicator.TryLogout(req);
                    if (res != null && res.IsSuccess())
                    {
                        _communicator.SetUserCredentials(default(KeyValuePair<string, string>));
                        Login_UpdateGUI();
                    }
                    else
                        MessageBox.Show(errorMsg);
                }
            }
            catch (Exception)
            {
                MessageBox.Show(errorMsg);
            }
        }

        private void signupBtn_Click(object sender, RoutedEventArgs e)
        {
            SignupWindow win = new SignupWindow(_communicator);
            win.Show();
            Close();
        }
    }
}
