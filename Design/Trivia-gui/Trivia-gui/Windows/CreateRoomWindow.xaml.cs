﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Communicator = Trivia_gui.Classes.Communicator;
using CreateRoomRequest = Trivia_gui.Classes.Request_CreateRoom;
using CreateRoomResponse = Trivia_gui.Classes.Response_CreateRoom;

namespace Trivia_gui.Windows
{
    /// <summary>
    /// Interaction logic for CreateRoomWindow.xaml
    /// </summary>
    public partial class CreateRoomWindow : Window
    {
        private Communicator _communicator;

        public CreateRoomWindow(Communicator comm)
        {
            InitializeComponent();
            _communicator = comm;
        }

        private void createroomBtn_Click(object sender, RoutedEventArgs e)
        {
            string errorMsg = "Signup request has failed!";
            try
            {
                if (_communicator.IsConnected)
                {
                    if (!roomnameBox.Text.IsEmpty() && !questionnumberBox.Text.IsEmpty() &&
                        !questiontimeBox.Text.IsEmpty() && !playernumberBox.Text.IsEmpty())
                    {
                        if (UInt32.TryParse(questiontimeBox.Text, out uint questionTime)
                            && UInt32.TryParse(playernumberBox.Text, out uint playerNumber)
                            && UInt32.TryParse(questionnumberBox.Text, out uint questionNumber))
                        {
                            CreateRoomRequest req = new CreateRoomRequest { answerTimeOut = questionTime, maxUsers = playerNumber, questionCount = questionNumber, roomName = roomnameBox.Text };
                            CreateRoomResponse res = _communicator.Send<CreateRoomRequest, CreateRoomResponse>(req);
                            if (res != null && res.IsSuccess())
                            {
                                //TODO Socket Stuff
                                MainWindow win = new MainWindow(_communicator);
                                win.Show();
                                Close();
                            }
                            else
                                MessageBox.Show(errorMsg);
                        }
                        else
                            MessageBox.Show("One of the input values in invalid!");
                    }
                    else
                    {
                        MessageBox.Show("The Fields must not be Empty!");
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show(errorMsg);
            }
        }

        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win = new MainWindow(_communicator);
            win.Show();
            Close();
        }
    }
}