﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Communicator = Trivia_gui.Classes.Communicator;
using JoinRoomRequest = Trivia_gui.Classes.Request_JoinRoom;
using JoinRoomResponse = Trivia_gui.Classes.Response_JoinRoom;
using GetRoomsRequest = Trivia_gui.Classes.Request_GetRooms;
using GetRoomsResponse = Trivia_gui.Classes.Response_GetRooms;
using GetRoomInfoRequest = Trivia_gui.Classes.Request_GetRoomState;
using GetRoomInfoResponse = Trivia_gui.Classes.Response_GetRoomState;
using RoomListControl = Trivia_gui.Classes.Controls.RoomListViewItem;

namespace Trivia_gui.Windows
{
    /// <summary>
    /// Interaction logic for JoinRoomWindow.xaml
    /// </summary>
    public partial class JoinRoomWindow : Window
    {
        private Communicator _communicator;

        private List<RoomListControl> _rooms;
        private bool stopSearch = false;

        public JoinRoomWindow(Communicator comm)
        {
            InitializeComponent();
            _communicator = comm;
            roomInfoPanel.Visibility = Visibility.Hidden;
            _rooms = new List<RoomListControl>();
            availableRooms.ItemsSource = _rooms;

            availableRooms.Dispatcher.Invoke( async () => await Refresh());
        }

        public async Task Refresh()
        {
            while (!stopSearch)
            {
                if (_communicator.IsConnected)
                {
                    try
                    {
                        _rooms.Clear();

                        var rooms = _communicator.Send<GetRoomsRequest, GetRoomsResponse>(new GetRoomsRequest());
                        foreach (var room in rooms.rooms)
                        {
                            GetRoomInfoRequest req = new GetRoomInfoRequest { roomId = room.id };
                            GetRoomInfoResponse res = _communicator.Send<GetRoomInfoRequest, GetRoomInfoResponse>(req);
                            if (!res.hasGameBegun)
                            {
                                _rooms.Add(new RoomListControl(room.id, room.name, res.players, res.adminName));
                            }
                        }
                        availableRooms.Items.Refresh();
                    }
                    catch (Exception)
                    {
                    }
                }
                await Task.Delay(Helper.REFRESH_COOLDOWN);
            }
        }

        private void availableRooms_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(availableRooms.SelectedIndex != -1)
            {
                var selRoom = availableRooms.SelectedItem as RoomListControl;
                playersList.ItemsSource = selRoom.Players;
                playersList.Items.Refresh();
            }
            else
            {
                roomInfoPanel.Visibility = Visibility.Hidden;
                playersList.Items.Clear();
            }
        }

        private void joinRoomBtn_Click(object sender, RoutedEventArgs e)
        {
            string errorMsg = "Join Room request has failed!";
            try
            {
                if (_communicator.IsConnected)
                {
                    var roomInfo = availableRooms.SelectedItem as RoomListControl;

                    JoinRoomRequest req = new JoinRoomRequest
                    {
                        roomId = (availableRooms.SelectedItem as RoomListControl).Id,
                        player = _communicator.UserCredentials.Key
                    };
                    JoinRoomResponse res = _communicator.Send<JoinRoomRequest, JoinRoomResponse>(req);

                    if (res != null && res.IsSuccess())
                    {
                        RoomLobbyWindow win = new RoomLobbyWindow(_communicator, roomInfo.Name, req.roomId, _communicator.UserCredentials.Key == roomInfo.AdminName);
                        win.Show();
                        Close();
                    }
                    else
                        MessageBox.Show(errorMsg);
                }
            }
            catch (Exception)
            {
                MessageBox.Show(errorMsg);
            }
        }

        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win = new MainWindow(_communicator);
            win.Show();
            Close();
        }
    }
}
