﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Communicator = Trivia_gui.Classes.Communicator;
using GetRoomInfoRequest = Trivia_gui.Classes.Request_GetRoomState;
using GetRoomInfoResponse = Trivia_gui.Classes.Response_GetRoomState;
using StartGameRequest = Trivia_gui.Classes.Request_StartGame;
using StartGameResponse = Trivia_gui.Classes.Response_StartGame;
using CloseRoomRequest = Trivia_gui.Classes.Request_CloseRoom;
using CloseRoomResponse = Trivia_gui.Classes.Response_CloseRoom;
using LeaveRoomRequest = Trivia_gui.Classes.Request_LeaveRoom;
using LeaveRoomResponse = Trivia_gui.Classes.Response_LeaveRoom;

namespace Trivia_gui.Windows
{
    /// <summary>
    /// Interaction logic for RoomLobbyWindow.xaml
    /// </summary>
    public partial class RoomLobbyWindow : Window
    {
        private Communicator _communicator;
        private uint _roomId;
        private List<TextBlock> _players;

        private bool stopSearch = false;

        public RoomLobbyWindow(Communicator comm, string roomName, uint roomId, bool isAdmin)
        {
            InitializeComponent();
            _communicator = comm;
            roomTxt.Text = roomName;
            _roomId = roomId;
            _players = new List<TextBlock>();
            playersList.ItemsSource = _players;

            if(isAdmin)
            {
                userMenuStk.Visibility = Visibility.Hidden;
            }
            else
            {
                adminMenuStk.Visibility = Visibility.Hidden;
            }

            roomTxt.Dispatcher.Invoke(async () => await Refresh());
        }

        private async Task Refresh()
        {
            while(!stopSearch)
            {
                if (_communicator.IsConnected)
                {
                    try
                    {
                        GetRoomInfoRequest req = new GetRoomInfoRequest { roomId = _roomId };
                        GetRoomInfoResponse res = _communicator.Send<GetRoomInfoRequest, GetRoomInfoResponse>(req);

                        if (res != null && res.IsSuccess())
                        {
                            playersNumTxt.Text = res.maxPlayers.ToString();
                            questionsNumTxt.Text = res.questionCount.ToString();
                            questionTimeTxt.Text = res.answerTimeout.ToString();
                            foreach (var pl in res.players)
                            {
                                _players.Add(new TextBlock
                                {
                                    Text = pl,
                                    FontSize = 12
                                });
                            }
                            playersList.Items.Refresh();
                        }
                    }
                    catch (Exception)
                    {
                    }
                    await Task.Delay(Helper.REFRESH_COOLDOWN);
                }
            }
        }

        private void leaveRoomBtn_Click(object sender, RoutedEventArgs e)
        {
            string errorMsg = "Leave Game request has failed!";
            try
            {
                if (_communicator.IsConnected)
                {
                    LeaveRoomRequest req = new LeaveRoomRequest { roomId = _roomId, username = _communicator.UserCredentials.Key };
                    LeaveRoomResponse res = _communicator.Send<LeaveRoomRequest, LeaveRoomResponse>(req);

                    if (res != null && res.IsSuccess())
                    {
                        MainWindow win = new MainWindow(_communicator);
                        win.Show();
                        Close();
                    }
                    else
                        MessageBox.Show(errorMsg);
                }
            }
            catch (Exception)
            {
                MessageBox.Show(errorMsg);
            }
        }

        private void closeRoomBtn_Click(object sender, RoutedEventArgs e)
        {
            string errorMsg = "Close Game request has failed!";
            try
            {
                if (_communicator.IsConnected)
                {
                    CloseRoomRequest req = new CloseRoomRequest { roomId = _roomId };
                    CloseRoomResponse res = _communicator.Send<CloseRoomRequest, CloseRoomResponse>(req);

                    if (res.IsSuccess())
                    {
                        MainWindow win = new MainWindow(_communicator);
                        win.Show();
                        Close();
                    }
                    else
                        MessageBox.Show(errorMsg);
                }
            }
            catch (Exception)
            {
                MessageBox.Show(errorMsg);
            }
        }
        
        private void startGameBtn_Click(object sender, RoutedEventArgs e)
        {
            string errorMsg = "Start Game request has failed!";
            try
            {
                if (_communicator.IsConnected)
                {
                    StartGameRequest req = new StartGameRequest { roomId = _roomId };
                    StartGameResponse res = _communicator.Send<StartGameRequest, StartGameResponse>(req);

                    if (res != null && res.IsSuccess())
                    {

                    }
                    else
                        MessageBox.Show(errorMsg);
                }
            }
            catch (Exception)
            {
                MessageBox.Show(errorMsg);
            }
        }
    }
}
