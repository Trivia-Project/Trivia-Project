﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Communicator = Trivia_gui.Classes.Communicator;
using SignupRequest = Trivia_gui.Classes.Request_Signup;
using SignupResponse = Trivia_gui.Classes.Response_Signup;

namespace Trivia_gui.Windows
{
    /// <summary>
    /// Interaction logic for SignupWindow.xaml
    /// </summary>
    public partial class SignupWindow : Window
    {
        private Communicator _communicator;
        public SignupWindow(Communicator comm)
        {
            InitializeComponent();
            _communicator = comm;
        }

        private void signupBtn_Click(object sender, RoutedEventArgs e)
        {
            string errorMsg = "Signup request has failed!";
            try
            {
                if (!passwordBox.Text.IsEmpty() && !usernameBox.Text.IsEmpty() && !emailBox.Text.IsEmpty())
                {
                    if (_communicator.IsConnected)
                    {
                        SignupRequest req = new SignupRequest { username = usernameBox.Text, password = passwordBox.Text, email = emailBox.Text };
                        SignupResponse res = _communicator.TrySignup(req);
                        if (res != null && res.IsSuccess())
                        {
                            MainWindow win = new MainWindow(_communicator);
                            win.Show();
                            Close();
                        }
                        else
                            MessageBox.Show(errorMsg);
                    }
                }
                else
                {
                    MessageBox.Show("Username, Password and Email must not be Empty!");
                }
            }
            catch (Exception)
            {
                MessageBox.Show(errorMsg);
            }
        }

        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win = new MainWindow(_communicator);
            win.Show();
            Close();
        }
    }
}
