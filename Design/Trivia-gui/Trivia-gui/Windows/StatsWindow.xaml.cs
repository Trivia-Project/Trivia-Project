﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Communicator = Trivia_gui.Classes.Communicator;
using PlayerStatsRequest = Trivia_gui.Classes.Request_GetPlayerStats;
using PlayerStatsResponse = Trivia_gui.Classes.Response_GetPlayerStats;

namespace Trivia_gui.Windows
{
    /// <summary>
    /// Interaction logic for StatsWindow.xaml
    /// </summary>
    public partial class StatsWindow : Window
    {
        private Communicator _communicator;

        public StatsWindow(Communicator comm)
        {
            InitializeComponent();
            _communicator = comm;
            userTxt.Text = _communicator.UserCredentials.Key;
            string errorMsg = "Player Stats request has failed!";
            try
            {
                if (_communicator.IsConnected)
                {
                    PlayerStatsRequest req = new PlayerStatsRequest { username = comm.UserCredentials.Key };
                    PlayerStatsResponse res = _communicator.Send<PlayerStatsRequest, PlayerStatsResponse>(req);

                    if (res != null && res.IsSuccess())
                    {
                        mistakesNumTxt.Text = res.mistakesCount.ToString();
                        gamesNumTxt.Text = res.gamesNum.ToString();
                        correctAnswersNumTxt.Text = res.correctAnswersCount.ToString();
                        averageAnswerTimeTxt.Text = $"{res.averageAnswerTime}ms";
                    }
                    else
                        MessageBox.Show(errorMsg);
                }
            }
            catch (Exception)
            {
                MessageBox.Show(errorMsg);
            }

        }

        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win = new MainWindow(_communicator);
            win.Show();
            Close();
        }
    }
}
