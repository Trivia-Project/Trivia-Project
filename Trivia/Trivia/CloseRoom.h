#pragma once

#pragma region includes
#pragma region natives
#pragma endregion

#pragma region customs
#include "Response.h"
#pragma endregion
#pragma endregion

struct CloseRoomRequest
{
	unsigned int roomId;
};

struct CloseRoomResponse : Response
{
};