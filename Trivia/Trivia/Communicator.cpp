#include "Communicator.h"
#include <exception>
#include <iostream>
#include <string>
#include "Response.h"
#include "ErrorResponse.h"

// using static const instead of macros 
static const unsigned short PORT = 8826;
static const unsigned int IFACE = 0;
static int counter = 0;


Communicator::Communicator()
{
	// notice that we step out to the global namespace
	// for the resolution of the function socket
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Communicator::~Communicator()
{

	// why is this try necessarily ?
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_socket);
	}
	catch (...) {}
}

void Communicator::serve()
{
	bindAndListen();

	// create new thread for handling message from queue of messages
	std::thread tr(&Communicator::handleRecievedMessages, this);
	tr.detach();
	

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers

		acceptClient();
	}
}

// listen to connecting requests from clients
// accept them, and create thread for each client
void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");


	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");

}

void Communicator::acceptClient()
{
	SOCKET client_socket = accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	m_clients.insert(std::make_pair(client_socket, m_handlerFactory.createLoginRequestHandler()));

	// create new thread for client	and detach from it
	std::thread tr(&Communicator::clientHandler, this, client_socket);
	tr.detach();

}


void Communicator::clientHandler(SOCKET client_socket)
{
	while (true)
	{
		counter++;
		Request* currRcvMsg = new Request;
		std::string response = {};
		int msgCode = Helper::getMessageTypeCode(client_socket);
		int size = Helper::getIntPartFromSocket(client_socket, 2);
		string data = Helper::getStringPartFromSocket(client_socket, size);
		//creating new request
		currRcvMsg->requestId = counter;
		currRcvMsg->buffer = Helper::stringToVector(data);
		currRcvMsg->code = msgCode;
		//TODO:: add recived time
		
		if (m_clients[client_socket]->isRequestRelevant(currRcvMsg->code))
		{
			RequestResult reqResult = m_clients[client_socket]->handleRequest(*currRcvMsg, currRcvMsg->code);
			m_clients[client_socket] = reqResult.newHandler;
			response = std::string(reqResult.response.begin(), reqResult.response.end());
		}
		else
		{
			response = "Invalid message type for current request handler!";
		}
		//send response
		Helper::sendData(client_socket, response);

		addRecievedMessage(currRcvMsg);
	}
}

void Communicator::addRecievedMessage(Request* msg)
{
	unique_lock<mutex> lck(_mtxRecievedMessages);

	_messageHandler.push(msg);
	lck.unlock();
	_msgQueueCondition.notify_all();

}




// remove the user from queue
void Communicator::safeDeleteUser(SOCKET id)
{

}

void Communicator::handleRecievedMessages()
{
	int msgCode = 0;
	SOCKET clientSock = 0;
	string userName;
	while (true)
	{
		try
		{
			unique_lock<mutex> lck(_mtxRecievedMessages);

			// Wait for clients to enter the queue.
			if (_messageHandler.empty())
				_msgQueueCondition.wait(lck);

			// in case the queue is empty.
			if (_messageHandler.empty())
				continue;

			Request* currMessage = _messageHandler.front();
			_messageHandler.pop();
			std::cout << Helper::vectorToString(currMessage->buffer) << std::endl;
			lck.unlock();

			// Extract the data from the tuple.
			//clientSock = currMessage->getSock();
			
			delete currMessage;
		}
		catch (...)
		{

		}
	}
}

