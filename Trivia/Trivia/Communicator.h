
#pragma once
#pragma comment (lib, "ws2_32.lib")
#include "WSAInitializer.h"

#include <WinSock2.h>
#include <ws2tcpip.h>
#include <Windows.h>
#include <iostream>
#include <string>
#include <thread>
#include <deque>
#include <queue>
#include <map>
#include "Helper.h"
#include <mutex>
#include <condition_variable>





#pragma region includes
#pragma region natives

#pragma endregion

#pragma region customs
#include "RequestHandlerFactory.h"
#pragma endregion
#pragma endregion

//LOLOL::typedef std::shared_ptr<IRequestHandler> MyPtr;
//LOLOL::typedef std::map<SOCKET, MyPtr> UsersMap;

class Communicator
{
public:
	Communicator();
	~Communicator();
	void serve();


private:

	void bindAndListen();
	void acceptClient();
	void clientHandler(SOCKET client_socket);
	void safeDeleteUser(SOCKET id);
	void handleRecievedMessages();
	void addRecievedMessage(Request*);
	

	RequestHandlerFactory m_handlerFactory;
	SOCKET _socket;


	// Queue for all clients. This way we will know who's the current writer.
	// SOCKET: client socket
	// string: userName
	map<SOCKET, IRequestHandler*> m_clients;
	//LOLOL::UsersMap m_clients;



	// Queue for messages - Will hold the mssage code and the file data. To add messages use std::ref<const ClientSocket>
	// SOCKET: client socket
	// string: message.
	std::queue<Request*> _messageHandler;

	std::mutex _mtxRecievedMessages;
	std::condition_variable _msgQueueCondition;

	// Wake up when an action has been finished.
	std::condition_variable _edited;

};
