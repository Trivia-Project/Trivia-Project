#pragma once

#pragma region includes
#pragma region natives
#include <string>
#pragma endregion

#pragma region customs
#include "Response.h"
#pragma endregion
#pragma endregion

struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeOut;
};

struct CreateRoomResponse : Response
{
};