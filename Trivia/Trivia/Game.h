#pragma once

#pragma region includes
#pragma region natives
#include <vector>
#include <map>
#pragma endregion

#pragma region customs
#include "Question.h"
#include "GameData.h"
#pragma endregion
#pragma endregion

class Game
{
public:
	Game();
	~Game();

	Question getQuestionForUser(std::string);
	void submitAnswer();
	void removePlayer();
private:
	std::vector<Question> m_questions;
	std::map<std::string, GameData> m_players;
};

