#pragma once

#pragma region includes
#pragma region natives
#pragma endregion

#pragma region customs
#include "Question.h"
#pragma endregion
#pragma endregion

struct GameData
{
	Question currentQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
};