#pragma once

#pragma region includes
#pragma region natives
#include <vector>
#pragma endregion

#pragma region customs
#include "IDatabase.h"
#include "Game.h"
#include "Room.h"
#pragma endregion
#pragma endregion

class GameManager
{
public:
	GameManager();
	~GameManager();

	Game createGame(Room);
	void deleteGame();
private:
	IDatabase* m_database;
	std::vector<Game> m_games;
};

