#include "GameRequestHandler.h"



GameRequestHandler::GameRequestHandler()
{
}


GameRequestHandler::~GameRequestHandler()
{
}

bool GameRequestHandler::isRequestRelevant(int)
{
	return false;
}
RequestResult GameRequestHandler::handleRequest(Request, int)
{
	return {};
}
RequestResult GameRequestHandler::getQuestion(Request)
{
	return {};
}
RequestResult GameRequestHandler::submitAnswer(Request)
{
	return {};
}
RequestResult GameRequestHandler::getGameResults(Request)
{
	return {};
}
RequestResult GameRequestHandler::leaveGame(Request)
{
	return {};
}