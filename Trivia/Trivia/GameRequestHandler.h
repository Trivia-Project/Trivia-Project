#pragma once

#pragma region includes
#pragma region natives
#pragma endregion

#pragma region customs
#include "IRequestHandler.h"
#include "Request.h"
#include "RequestResult.h"
#include "Game.h"
#include "GameManager.h"
#pragma endregion
#pragma endregion

class RequestHandlerFactory;

class GameRequestHandler :
	public IRequestHandler
{
public:
	GameRequestHandler();
	~GameRequestHandler();

	virtual bool isRequestRelevant(int);
	virtual RequestResult handleRequest(Request, int);
private:
	Game* m_game;
	std::string* m_user;
	GameManager* m_gameManager;
	RequestHandlerFactory* m_handlerFactory;
	
	RequestResult getQuestion(Request);
	RequestResult submitAnswer(Request);
	RequestResult getGameResults(Request);
	RequestResult leaveGame(Request);
};

