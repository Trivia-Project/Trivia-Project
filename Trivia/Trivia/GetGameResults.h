#pragma once

#pragma region includes
#pragma region natives
#include <vector>
#pragma endregion

#pragma region customs
#include "Response.h"
#pragma endregion
#pragma endregion

struct GetGameResultsRequest
{
	unsigned int roomId;
};

struct GetGameResultsResponse : Response
{
	std::vector<PlayerResults> results;
};

struct PlayerResults
{
	unsigned int correctAnswerCount;
	unsigned int mistakesCount;
	unsigned int averageAnswerTime;
};