#pragma once

#pragma region includes
#pragma region natives
#include <map>
#pragma endregion

#pragma region customs
#include "Response.h"
#pragma endregion
#pragma endregion

struct GetHighscoresRequest
{

};

struct GetHighscoresResponse : Response
{
	std::map<std::string, int> highscores;
};