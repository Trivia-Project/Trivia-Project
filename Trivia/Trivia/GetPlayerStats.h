#pragma once

#pragma region includes
#pragma region natives
#include <string>
#pragma endregion

#pragma region customs
#include "Response.h"
#pragma endregion
#pragma endregion

struct GetPlayerStatsRequest
{
	std::string username;
};

struct GetPlayerStatsResponse : Response
{
	unsigned int gamesNum;
	unsigned int correctAnswerCount;
	unsigned int mistakesCount;
	unsigned int averageAnswerTime;
};