#pragma once

#pragma region includes
#pragma region natives
#include <map>
#pragma endregion

#pragma region customs
#include "Response.h"
#pragma endregion
#pragma endregion

struct GetQuestionRequest
{

};

struct GetQuestionResponse : Response
{
	std::string question;
	std::map<unsigned int, std::string> answers;
};