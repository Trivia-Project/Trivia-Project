#pragma once

#pragma region includes
#pragma region natives
#include <vector>
#pragma endregion

#pragma region customs
#include "RoomData.h"
#include "Response.h"
#pragma endregion
#pragma endregion

struct GetRoomStateRequest
{
	unsigned int roomId;
};

struct GetRoomStateResponse : Response
{
	unsigned int questionCount;
	unsigned int answerTimeout;
	unsigned int maxPlayers;
	bool hasGameBegun;
	std::vector<std::string> players;
};