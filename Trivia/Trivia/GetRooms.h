#pragma once

#pragma region includes
#pragma region natives
#include <vector>
#pragma endregion

#pragma region customs
#include "RoomData.h"
#include "Response.h"
#pragma endregion
#pragma endregion

struct GetRoomsRequest
{

};

struct GetRoomsResponse : Response
{
	std::vector<RoomData> rooms;
};