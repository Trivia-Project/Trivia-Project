#pragma once

#pragma region includes
#pragma region natives
#include <vector>
#pragma endregion

#pragma region customs
#include "SqliteDatabase.h"
#pragma endregion
#pragma endregion

class HighscoreTable
{
public:
	HighscoreTable();
	~HighscoreTable();

	std::map<std::string, int> getHighscores();
private:
	IDatabase* m_database;
};

