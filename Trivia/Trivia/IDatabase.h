#pragma once

#pragma region includes
#pragma region natives
#include <map>
#include <list>
#pragma endregion

#pragma region customs
#include "Question.h"
#pragma endregion
#pragma endregion

class IDatabase
{
public:
	IDatabase();
	~IDatabase();

	virtual std::map<std::string, int> getHighscores() = 0;
	virtual bool doesUserExist(std::string) = 0;
	virtual std::list<Question> getQuestions(int) = 0;
	virtual bool tryLogin(std::string, std::string) = 0;
	virtual bool trySignup(std::string, std::string, std::string) = 0;
};