#pragma once

#pragma region includes
#pragma region natives
#pragma endregion

#pragma region customs
#include "Request.h"
#include "RequestResult.h"
#pragma endregion
#pragma endregion

struct RequestResult;

class IRequestHandler
{
public:
	IRequestHandler();
	~IRequestHandler();
	virtual bool isRequestRelevant(int) = 0;
	virtual RequestResult handleRequest(Request, int) = 0;
	//static int getRequestCode(std::vector<char>);
};