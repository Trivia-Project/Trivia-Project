#pragma once

#pragma region includes
#pragma region natives
#include <string>
#pragma endregion

#pragma region customs
#include "Response.h"
#pragma endregion
#pragma endregion

struct JoinRoomRequest
{
	unsigned int roomId;
	std::string player;
};

struct JoinRoomResponse : Response
{
};