#pragma once

#pragma region includes
#pragma region natives
#include <vector>
#pragma endregion

#pragma region customs
#include "json.hpp"
#include "Login.h"
#include "Signup.h"
#include "JoinRoom.h"
#include "CreateRoom.h"
#include "SubmitAnswer.h"
#include "json.hpp"
#include "Helper.h"
#include "ErrorResponse.h"
#include "Login.h"
#include "Signup.h"
#include "Logout.h"
#include "GetRooms.h"
#include "JoinRoom.h"
#include "CreateRoom.h"
#include "GetHighscores.h"
#include "CloseRoom.h"
#include "StartGame.h"
#include "GetRoomState.h"
#include "LeaveRoom.h"
#include "SubmitAnswer.h"
#pragma endregion
#pragma endregion

using json = nlohmann::json;

struct JsonRequestPacketDeserializer
{
	static LoginRequest deserializeLoginRequest(std::vector<char> vec) //vector<char> => buffer
	{
		std::string dumped = Helper::vectorToString(vec);
		json j = json::parse(dumped);
		LoginRequest ret = {j["username"],j["password"]};
		return ret;
	}
	static SignupRequest deserializeSignupRequest(std::vector<char> vec)
	{
		std::string dumped = Helper::vectorToString(vec);
		json j = json::parse(dumped);
		SignupRequest ret = { j["username"],j["password"], j["email"] };

		return ret;
	}
	static LogoutRequest deserializeLogoutRequest(std::vector<char>)
	{
		return {};
	}
	static GetRoomsRequest deserializeGetRoomsRequest(std::vector<char>)
	{
		return {};
	}
	static JoinRoomRequest deserializeJoinRoomRequest(std::vector<char>)
	{
		return {};
	}
	static CreateRoomRequest deserializeCreateRoomRequest(std::vector<char>)
	{
		return {};
	}
	static GetHighscoresRequest deserializeHighscoreRequest(std::vector<char>)
	{
		return {};
	}
	static CloseRoomRequest deserializeCloseRoomRequest(std::vector<char>)
	{
		return {};
	}
	static StartGameRequest deserializeStartGameRequest(std::vector<char>)
	{
		return {};
	}
	static GetRoomStateRequest deserializeGetRoomStateRequest(std::vector<char>)
	{
		return {};
	}
	static LeaveRoomRequest deserializeLeaveRoomRequest(std::vector<char>)
	{
		return {};
	}
	static SubmitAnswerRequest deserializeSubmitAnswerRequest(std::vector<char>)
	{
		return {};
	}
};

struct JsonResponsePacketSerializer
{
	static std::vector<char> serializeResponse(ErrorResponse r)
	{
		json j;
		j["message"] = r.message;
		return Helper::stringToVector(j.dump());
	}
	static std::vector<char> serializeResponse(LoginResponse r)
	{
		json j;
		j["status"] = r.status;
		return Helper::stringToVector(j.dump());
	}
	static std::vector<char> serializeResponse(SignupResponse r)
	{
		json j;
		j["status"] = r.status;
		return Helper::stringToVector(j.dump());
	}
	static std::vector<char> serializeResponse(LogoutResponse)
	{
		json j;
		return Helper::stringToVector(j.dump());
	}
	static std::vector<char> serializeResponse(GetRoomsResponse)
	{
		json j;
		return Helper::stringToVector(j.dump());
	}
	static std::vector<char> serializeResponse(JoinRoomResponse)
	{
		json j;
		return Helper::stringToVector(j.dump());
	}
	static std::vector<char> serializeResponse(CreateRoomResponse)
	{
		json j;
		return Helper::stringToVector(j.dump());
	}
	static std::vector<char> serializeResponse(GetHighscoresResponse)
	{
		json j;
		return Helper::stringToVector(j.dump());
	}
	static std::vector<char> serializeResponse(CloseRoomResponse)
	{
		json j;
		return Helper::stringToVector(j.dump());
	}
	static std::vector<char> serializeResponse(StartGameResponse)
	{
		json j;
		return Helper::stringToVector(j.dump());
	}
	static std::vector<char> serializeResponse(GetRoomStateResponse)
	{
		json j;
		return Helper::stringToVector(j.dump());
	}
	static std::vector<char> serializeResponse(LeaveRoomResponse)
	{
		json j;
		return Helper::stringToVector(j.dump());
	}
	static std::vector<char> serializeResponse(SubmitAnswerResponse)
	{
		json j;
		return Helper::stringToVector(j.dump());
	}
};