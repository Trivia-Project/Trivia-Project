#include "LoginManager.h"
#include <iostream>
#include <string>
#include "SqliteDatabase.h"


LoginManager::LoginManager(RequestHandlerFactory* handlerFac) : m_handlerFactory(handlerFac)
{
	m_database = new SqliteDatabase();
}


LoginManager::~LoginManager()
{
}

bool LoginManager::signup(std::string username, std::string password, std::string email)
{
	if (m_database->doesUserExist(username))
	{
		std::cout << "ERROR 412 : User " + username + " already exists!";
		return false;
	}
	else
	{
		if (m_database->trySignup(username, password, email))
		{
			std::cout << "Successfully created account " + username;
			return true;
		}
		else
		{
			std::cout << "An Error has occured";
			return false;
		}
	}
}
bool LoginManager::login(std::string username, std::string password)
{
	if (m_database->doesUserExist(username))
	{
		if (m_database->tryLogin(username, password))
		{
			std::string loggedUser(username);
			m_loggedUsers.push_back(loggedUser);
			return true;
		}
		else
		{
			std::cout << "ERROR 402 : Invalid password for user " + username;
			return false;
		}
	}
	else
	{
		std::cout << "ERROR 404 : User " + username + " wasn't found!";
		return false;
	}

}
bool LoginManager::logout()
{
	return true;
}