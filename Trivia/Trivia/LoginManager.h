#pragma once

#pragma region includes
#pragma region natives
#include <vector>
#pragma endregion

#pragma region customs
#include "IDatabase.h"
#pragma endregion
#pragma endregion

class RequestHandlerFactory;

class LoginManager
{
public:
	LoginManager(RequestHandlerFactory*);
	~LoginManager();

	bool signup(std::string, std::string, std::string);
	bool login(std::string, std::string);
	bool logout();
private:
	IDatabase* m_database;
	RequestHandlerFactory* m_handlerFactory;
	std::vector<std::string> m_loggedUsers;
};

