#include "LoginRequestHandler.h"
#include "Login.h"
#include "Signup.h"
#include "JsonService.h"
#include "RequestHandlerFactory.h"
#include "define.h"


LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory* handleFactory, LoginManager* loginMngr) :
	m_handleFactory(handleFactory), m_loginManager(loginMngr)
{
}


LoginRequestHandler::~LoginRequestHandler()
{
}

bool LoginRequestHandler::isRequestRelevant(int code)
{
	return  code == MT_CLIENT_LOG_IN || code == MT_CLIENT_SIGN_UP;
}
RequestResult LoginRequestHandler::handleRequest(Request req, int code)
{
	switch (code)
	{
	case MT_CLIENT_LOG_IN:
		return login(req);
	case MT_CLIENT_SIGN_UP:
		return signup(req);
	default:
		return {};
	}
}
RequestResult LoginRequestHandler::login(Request req)
{
	LoginResponse response = { };
	LoginRequest loginReq = JsonRequestPacketDeserializer::deserializeLoginRequest(req.buffer);
	if (m_loginManager->login(loginReq.username, loginReq.password))
	{
		response.status = SUCCESS;
	}
	else
	{
		response.status = FAILED;
	}
	std::vector<char> buffer = JsonResponsePacketSerializer::serializeResponse(response);
	RequestResult reqRes = { buffer };

	if (response.status == FAILED)
	{
		reqRes.newHandler = this;
	}
	else
	{
		reqRes.newHandler = m_handleFactory->createMenuRequestHandler(&loginReq.username);
	}
	return reqRes;
}
RequestResult LoginRequestHandler::signup(Request req)
{
	SignupResponse response = {};
	SignupRequest signupReq = JsonRequestPacketDeserializer::deserializeSignupRequest(req.buffer);
	if (m_loginManager->signup(signupReq.username, signupReq.password, signupReq.email))
	{
		response.status = SUCCESS;
	}
	else
	{
		response.status = FAILED;
	}
	std::vector<char> buffer = JsonResponsePacketSerializer::serializeResponse(response);
	RequestResult reqRes = { buffer, this };
	return reqRes;
}