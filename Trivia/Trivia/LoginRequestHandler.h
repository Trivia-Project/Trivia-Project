#pragma once

#pragma region includes
#pragma region natives
#pragma endregion

#pragma region customs
#include "RequestResult.h"
#include "Request.h"
#pragma endregion
#pragma endregion

class RequestHandlerFactory;
class LoginManager;

class LoginRequestHandler :
	public IRequestHandler
{
public:
	LoginRequestHandler(RequestHandlerFactory*, LoginManager*);
	~LoginRequestHandler();
	virtual bool isRequestRelevant(int);
	virtual RequestResult handleRequest(Request, int);
private:
	LoginManager* m_loginManager;
	RequestHandlerFactory* m_handleFactory;

	RequestResult login(Request);
	RequestResult signup(Request);
};

