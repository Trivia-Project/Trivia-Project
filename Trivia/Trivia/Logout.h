#pragma once

#pragma region includes
#pragma region natives
#include <string>
#pragma endregion

#pragma region customs
#include "Response.h"
#pragma endregion
#pragma endregion

struct LogoutRequest
{
	std::string username;
};

struct LogoutResponse : Response
{
};