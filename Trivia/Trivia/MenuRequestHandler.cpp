#include "MenuRequestHandler.h"
#include "define.h"


MenuRequestHandler::MenuRequestHandler(std::string* username, RoomManager* roomMngr, RequestHandlerFactory* handlerFactory)
	: m_user(username), m_roomManager(roomMngr), m_handlerFactory(handlerFactory)
{
	m_highscoreTable = new HighscoreTable();
}


MenuRequestHandler::~MenuRequestHandler()
{
}

bool MenuRequestHandler::isRequestRelevant(int code)
{
	return  code == MT_CLIENT_LOGOUT || code == MT_CLIENT_GET_ROOMS || code == MT_CLIENT_GET_ROOM_STATE || 
		code == MT_CLIENT_GET_PLAYER_STATS || code == MT_CLIENT_GET_HIGHSCORES || code == MT_CLIENT_JOIN_ROOM || code == MT_CLIENT_CREATE_ROOM;
}
RequestResult MenuRequestHandler::handleRequest(Request req, int code)
{
	switch (code)
	{
	case MT_CLIENT_LOGOUT:
		return signout(req);
	case MT_CLIENT_GET_ROOMS:
		return getRooms(req);
	case MT_CLIENT_GET_ROOM_STATE:
		return getRoomState(req);
	case MT_CLIENT_GET_PLAYER_STATS:
		return getPlayerStats(req);
	case MT_CLIENT_GET_HIGHSCORES:
		return getHighscores(req);
	case MT_CLIENT_JOIN_ROOM:
		return joinRoom(req);
	case MT_CLIENT_CREATE_ROOM:
		return createRoom(req);
	default:
		return {};
	}
}
RequestResult MenuRequestHandler::signout(Request)
{
	return {};
}
RequestResult MenuRequestHandler::getRooms(Request)
{
	return {};
}
RequestResult MenuRequestHandler::getRoomState(Request)
{
	return {};
}
RequestResult MenuRequestHandler::getPlayerStats(Request)
{
	return {};
}
RequestResult MenuRequestHandler::getHighscores(Request)
{
	return {};
}
RequestResult MenuRequestHandler::joinRoom(Request)
{
	return {};
}
RequestResult MenuRequestHandler::createRoom(Request)
{
	return {};
}
