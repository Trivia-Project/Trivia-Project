#pragma once

#pragma region includes
#pragma region natives
#pragma endregion

#pragma region customs
#include "IRequestHandler.h"
#include "RoomManager.h"
#include "HighscoreTable.h"
#include "Request.h"
#include "RequestResult.h"
#pragma endregion
#pragma endregion

class RequestHandlerFactory;

class MenuRequestHandler :
	public IRequestHandler
{
public:
	MenuRequestHandler(std::string*, RoomManager*, RequestHandlerFactory*);
	~MenuRequestHandler();

	virtual bool isRequestRelevant(int);
	virtual RequestResult handleRequest(Request, int);
private:
	std::string* m_user;
	RoomManager* m_roomManager;
	HighscoreTable* m_highscoreTable;
	RequestHandlerFactory* m_handlerFactory;

	RequestResult signout(Request);
	RequestResult getRooms(Request);
	RequestResult getRoomState(Request);
	RequestResult getPlayerStats(Request);
	RequestResult getHighscores(Request);
	RequestResult joinRoom(Request);
	RequestResult createRoom(Request);
};

