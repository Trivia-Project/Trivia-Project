#include "Question.h"



Question::Question(std::string q, std::string ans, std::string ans2, std::string ans3, std::string ans4) : 
	m_correctAnswer(ans), m_question(q)
{
	m_possibleAnswers = {};
	m_possibleAnswers.push_back(ans);
	m_possibleAnswers.push_back(ans2);
	m_possibleAnswers.push_back(ans3);
	m_possibleAnswers.push_back(ans4);
}


Question::~Question()
{
}

std::string Question::getQuestion()
{
	return m_question;
}
std::vector<std::string> Question::getPossibleAnswers()
{
	return m_possibleAnswers;
}
std::string Question::getCorrectAnswer()
{
	return m_correctAnswer;
}