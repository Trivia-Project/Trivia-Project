#pragma once

#pragma region includes
#pragma region natives
#include <vector>
#pragma endregion

#pragma region customs
#pragma endregion
#pragma endregion

class Question
{
public:
	Question(std::string, std::string, std::string, std::string, std::string);
	~Question();

	std::string getQuestion();
	std::vector<std::string> getPossibleAnswers();
	std::string getCorrectAnswer();

private:
	std::string m_question;
	std::string m_correctAnswer;
	std::vector<std::string> m_possibleAnswers;
};

