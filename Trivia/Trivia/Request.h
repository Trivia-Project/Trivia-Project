#pragma once

#pragma region includes
#pragma region natives
#include <ctime>
#include <vector>
#pragma endregion

#pragma region customs
#pragma endregion
#pragma endregion

struct Request
{
	unsigned int requestId;
	std::time_t receivalTime;
	int code;
	std::vector<char> buffer;
};