#include "RequestHandlerFactory.h"



RequestHandlerFactory::RequestHandlerFactory()
{
	m_loginManager = new LoginManager(this);
}


RequestHandlerFactory::~RequestHandlerFactory()
{
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(this, m_loginManager);
}
MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(std::string* user)
{
	return new MenuRequestHandler(user, m_roomManager, this);
}
RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler()
{
	return {};
}
RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler()
{
	return {};
}
GameRequestHandler* RequestHandlerFactory::createGameRequestHandler()
{
	return {};
}