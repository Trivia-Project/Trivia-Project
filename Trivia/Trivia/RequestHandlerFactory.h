#pragma once

#pragma region includes
#pragma region natives
#pragma endregion

#pragma region customs
#include "LoginManager.h"
#include "RoomManager.h"
#include "GameManager.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameRequestHandler.h"
#pragma endregion
#pragma endregion

class RequestHandlerFactory
{
public:
	RequestHandlerFactory();
	~RequestHandlerFactory();

	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(std::string*);
	RoomAdminRequestHandler* createRoomAdminRequestHandler();
	RoomMemberRequestHandler* createRoomMemberRequestHandler();
	GameRequestHandler* createGameRequestHandler();
private:
	LoginManager* m_loginManager;
	RoomManager* m_roomManager;
	GameManager* m_gameManager;
};

