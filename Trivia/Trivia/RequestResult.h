#pragma once

#pragma region includes
#pragma region natives
#include <vector>
#pragma endregion

#pragma region customs
#include "IRequestHandler.h"
#pragma endregion
#pragma endregion

class IRequestHandler;

struct RequestResult
{
	std::vector<char> response; //vector<char> => buffer
	IRequestHandler* newHandler;
};