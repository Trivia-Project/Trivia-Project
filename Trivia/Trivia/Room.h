#pragma once

#pragma region includes
#pragma region natives
#include <vector>
#pragma endregion

#pragma region customs
#include "RoomData.h"
#pragma endregion
#pragma endregion

class Room
{
public:
	Room();
	Room(std::string, unsigned int, unsigned int, unsigned int);
	~Room();

	void addUser();
	void removeUser();
	std::vector<std::string> getAllUsers();
	RoomData getMetaData();
private:
	RoomData m_metadata;
	std::vector<std::string> m_users;
};