#include "RoomAdminRequestHandler.h"



RoomAdminRequestHandler::RoomAdminRequestHandler()
{
}


RoomAdminRequestHandler::~RoomAdminRequestHandler()
{
}

bool RoomAdminRequestHandler::isRequestRelevant(int)
{
	return false;
}
RequestResult RoomAdminRequestHandler::handleRequest(Request, int)
{
	return {};
}
RequestResult RoomAdminRequestHandler::closeRoom(Request)
{
	return {};
}
RequestResult RoomAdminRequestHandler::startGame(Request)
{
	return {};
}
RequestResult RoomAdminRequestHandler::getRoomState(Request)
{
	return {};
}