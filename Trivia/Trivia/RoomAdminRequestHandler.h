#pragma once

#pragma region includes
#pragma region natives
#pragma endregion

#pragma region customs
#include "IRequestHandler.h"
#include "Room.h"

#include "RoomManager.h"
#include "Request.h"
#include "RequestResult.h"
#pragma endregion
#pragma endregion

class RequestHandlerFactory;

class RoomAdminRequestHandler :
	public IRequestHandler
{
public:
	RoomAdminRequestHandler();
	~RoomAdminRequestHandler();

	virtual bool isRequestRelevant(int);
	virtual RequestResult handleRequest(Request, int);
private:
	Room* m_room;
	std::string* m_user;
	RoomManager* m_roomManager;
	RequestHandlerFactory* m_handlerFactory;

	RequestResult closeRoom(Request);
	RequestResult startGame(Request);
	RequestResult getRoomState(Request);
};

