#include "RoomManager.h"



RoomManager::RoomManager()
{
}


RoomManager::~RoomManager()
{
}

std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> vec = {};
	for each (auto var in m_rooms)
	{
		vec.push_back(var.second.getMetaData());
	}

	return vec;
}
unsigned int RoomManager::getRoomState(int) //int => id
{
	return 0;
}
void RoomManager::deleteRoom(int id) //int => id
{
	m_rooms.erase(id);
}
void RoomManager::createRoom(std::string name, unsigned int maxP, unsigned int timePerQuestion)
{
	Room r(name, maxP, timePerQuestion, m_rooms.size() + 1);
	m_rooms[m_rooms.size() + 1] = r;
}