#pragma once

#pragma region includes
#pragma region natives
#include <map>
#include <vector>
#pragma endregion

#pragma region customs
#include "Room.h"
#include "RoomData.h"
#pragma endregion
#pragma endregion

class RoomManager
{
public:
	RoomManager();
	~RoomManager();

	std::vector<RoomData> getRooms();
	unsigned int getRoomState(int); //int => id
	void deleteRoom(int);
	void createRoom(std::string, unsigned int, unsigned int);
private:
	std::map<int, Room> m_rooms; //int => id
};

