#include "RoomMemberRequestHandler.h"



RoomMemberRequestHandler::RoomMemberRequestHandler()
{
}


RoomMemberRequestHandler::~RoomMemberRequestHandler()
{
}

bool RoomMemberRequestHandler::isRequestRelevant(int)
{
	return false;
}
RequestResult RoomMemberRequestHandler::handleRequest(Request, int)
{
	return {};
}
RequestResult RoomMemberRequestHandler::leaveRoom(Request)
{
	return {};
}
RequestResult RoomMemberRequestHandler::getRoomState(Request)
{
	return {};
}
