#pragma once

#pragma region includes
#pragma region natives
#pragma endregion

#pragma region customs
#include "SqliteDatabase.h"
#include "Communicator.h"
#include "RequestHandlerFactory.h"
#pragma endregion
#pragma endregion

class Server
{
public:
	Server();
	~Server();

	void run();
private:
	SqliteDatabase m_database;
	Communicator m_communicator;
	RequestHandlerFactory* m_handlerFactory;
};

