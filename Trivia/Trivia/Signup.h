#pragma once

#pragma region includes
#pragma region natives
#include <string>
#pragma endregion

#pragma region customs
#include "Response.h"
#pragma endregion
#pragma endregion

struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
};

struct SignupResponse : Response
{
};