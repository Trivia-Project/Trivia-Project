#include "SqliteDatabase.h"
#include <string>
#include <iostream>


#pragma region callbacks

static int create_callback(void *NotUsed, int argc, char **argv, char **azColName)
{
	int i;
	for (i = 0; i<argc; i++) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}

static int select_callback(void *data, int argc, char **argv, char **azColName)
{
	int i;
	fprintf(stderr, "%s: ", (const char*)data);

	for (i = 0; i<argc; i++) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}

	printf("\n");
	return 0;
}

static int insert_callback(void *NotUsed, int argc, char **argv, char **azColName)
{
	int i;
	for (i = 0; i<argc; i++) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}

static int delete_callback(void *data, int argc, char **argv, char **azColName)
{
	int i;
	fprintf(stderr, "%s: ", (const char*)data);

	for (i = 0; i<argc; i++) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}

#pragma endregion


SqliteDatabase::SqliteDatabase() : IDatabase()
{
	_errMsg = 0;
	std::string query = "";
	int res = sqlite3_open(DATABASE_FILE_NAME, &_db);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed to access the Database!" << std::endl;
	}
	else
	{
		query = "CREATE TABLE IF NOT EXISTS GAMES(" \
			"GAME_ID INT PRIMARY KEY," \
			"STATUS INT NOT NULL," \
			"START_TIME DATETIME NOT NULL," \
			"END_TIME DATETIME NOT NULL);";

		res = sqlite3_exec(_db, query.c_str(), create_callback, 0, &_errMsg);

		query = "CREATE TABLE IF NOT EXISTS QUESTIONS(" \
			"QUESTION_ID INT PRIMARY KEY," \
			"QUESTION TEXT NOT NULL," \
			"CORRECT_ANS TEXT NOT NULL," \
			"ANS2 TEXT NOT NULL," \
			"ANS3 TEXT NOT NULL," \
			"ANS4 TEXT NOT NULL);";

		res = sqlite3_exec(_db, query.c_str(), create_callback, 0, &_errMsg);

		query = "CREATE TABLE IF NOT EXISTS USERS(" \
			"USERNAME TEXT PRIMARY KEY NOT NULL," \
			"PASSWORD TEXT NOT NULL," \
			"EMAIL TEXT NOT NULL," \
			"SCORE INT NOT NULL);";

		res = sqlite3_exec(_db, query.c_str(), create_callback, 0, &_errMsg);
	}
}


SqliteDatabase::~SqliteDatabase()
{
}

std::map<std::string, int> SqliteDatabase::getHighscores()
{
	std::string query = "SELECT USERNAME, SCORE FROM USERS;";
	std::map<std::string, int> m = {};

	sqlite3_prepare(_db, query.c_str(), -1, &_stmt, NULL);//getting pics
	sqlite3_step(_stmt);//executing the statement
	while (sqlite3_column_text(_stmt, 0)) //rows
	{
		std::string username = NULL;
		int score = 0;
		for (int i = 1; i < 6; i++) // columns
		{
			switch (i)
			{
			case 0:
				username = (char *)sqlite3_column_text(_stmt, i);
				break;
			case 1: //path
				score = std::atoi((char *)sqlite3_column_text(_stmt, i));
				break;
			default:
				break;
			}
		}
		m.insert(std::make_pair(username, score));
		sqlite3_step(_stmt);
	}

	sqlite3_finalize(_stmt);
	return m;
}
bool SqliteDatabase::doesUserExist(std::string username)
{
	std::string query = "SELECT count(1) FROM USERS WHERE USERNAME=\"" + username + "\";";
	sqlite3_prepare(_db, query.c_str(), -1, &_stmt, NULL);//getting albums
	sqlite3_step(_stmt);//executing the statement
	int count = 0;
	while (sqlite3_column_text(_stmt, 0)) //rows
	{
		count = std::atoi((char *)sqlite3_column_text(_stmt, 0));
		sqlite3_step(_stmt);//executing the statement
	}
	sqlite3_finalize(_stmt);
	return count > 0;
}
std::list<Question> SqliteDatabase::getQuestions(int)
{
	std::string query = "SELECT * FROM QUESTIONS;";
	std::list<Question> l = {};

	sqlite3_prepare(_db, query.c_str(), -1, &_stmt, NULL);//getting pics
	sqlite3_step(_stmt);//executing the statement
	while (sqlite3_column_text(_stmt, 0)) //rows
	{
		std::string q, ans, ans2, ans3, ans4;
		for (int i = 1; i < 6; i++) // columns
		{
			switch (i)
			{
			case 1: 
				q = (char *)sqlite3_column_text(_stmt, i);
				break;
			case 2: //path
				ans = (char *)sqlite3_column_text(_stmt, i);
				break;
			case 3: //creation date
				ans2 = (char *)sqlite3_column_text(_stmt, i);
				break;
			case 4: //creation date
				ans3 = (char *)sqlite3_column_text(_stmt, i);
				break;
			case 5: //creation date
				ans4 = (char *)sqlite3_column_text(_stmt, i);
				break;
			default:
				break;
			}
		}

		Question a(q, ans, ans2, ans3, ans4);
		l.push_back(a);
		sqlite3_step(_stmt);
	}

	sqlite3_finalize(_stmt);
	return l;
}

bool SqliteDatabase::tryLogin(std::string username, std::string password)
{
	std::string query = "SELECT count(1) FROM USERS WHERE USERNAME=\"" + username + "\" AND PASSWORD=\"" + password + "\";";
	sqlite3_prepare(_db, query.c_str(), -1, &_stmt, NULL);//getting albums
	sqlite3_step(_stmt);//executing the statement
	int count = 0;
	while (sqlite3_column_text(_stmt, 0)) //rows
	{
		count = std::atoi((char *)sqlite3_column_text(_stmt, 0));
		sqlite3_step(_stmt);//executing the statement
	}
	sqlite3_finalize(_stmt);
	return count > 0;
}

bool SqliteDatabase::trySignup(std::string username, std::string password, std::string email)
{
	std::string query = "INSERT INTO USERS(USERNAME,PASSWORD,EMAIL,SCORE) " \
		"VALUES(\"" + username + "\",\"" + password + "\",\"" + email + "\",0);";
	int res = sqlite3_exec(_db, query.c_str(), insert_callback, 0, &_errMsg);
	return res == SQLITE_OK;
}