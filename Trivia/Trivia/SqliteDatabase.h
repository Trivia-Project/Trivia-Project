#pragma once

#define DATABASE_FILE_NAME "trivia.db"

#pragma region includes
#pragma region natives
#include <map>
#include <memory>
#pragma endregion

#pragma region customs
#include "IDatabase.h"
#include "sqlite3.h"
#pragma endregion
#pragma endregion

class SqliteDatabase :
	public IDatabase
{
public:
	SqliteDatabase();
	~SqliteDatabase();

	virtual std::map<std::string, int> getHighscores();
	virtual bool doesUserExist(std::string);
	virtual std::list<Question> getQuestions(int);
	virtual bool tryLogin(std::string, std::string);
	virtual bool trySignup(std::string, std::string, std::string);
private:
	sqlite3* _db;
	sqlite3_stmt* _stmt;
	char* _errMsg;
};

