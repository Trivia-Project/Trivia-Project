#pragma once

#pragma region includes
#pragma region natives
#pragma endregion

#pragma region customs
#include "Response.h"
#pragma endregion
#pragma endregion


struct SubmitAnswerRequest
{
	unsigned int answerId;
};

struct SubmitAnswerResponse : Response
{
	unsigned int correctAnswerId;
};