#pragma once

#pragma region includes
#pragma region natives
#include <string>
#pragma endregion

#pragma region customs
#pragma endregion
#pragma endregion

struct User
{
	std::string username;
	std::string password;
	std::string email;
};