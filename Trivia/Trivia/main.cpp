#pragma region includes
#pragma region natives
#include <string>
#pragma endregion

#pragma region customs
#include "Communicator.h"
#include "LoginRequestHandler.h"
#pragma endregion
#pragma endregion

int main(void)
{
	WSAInitializer a;
	Communicator com;
	com.serve();
	getchar();
	return 0;
}